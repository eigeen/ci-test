﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.Sqlite;

namespace WpfTest2_MVVM.DataAccess
{
    class SqliteLink
    {
        private SqliteConnection conn;
        private SqliteCommand cmd;
        private SqliteDataReader reader;

        public SqliteLink(string dbPath)
        {
            conn = new SqliteConnection($"Data Source={dbPath}");
            conn.Open();
            cmd = conn.CreateCommand();
        }

        public void CreateTable()
        {
            cmd.CommandText = "CREATE TABLE Students (ID TEXT NOT NULL, Name TEXT NOT NULL, Sex TEXT NOT NULL, Age INTEGER NOT NULL, Score REAL, GPA REAL)";
            cmd.ExecuteNonQuery();
        }
        public SqliteDataReader Reader(string command)
        {
            cmd.CommandText = command;
            reader = cmd.ExecuteReader();
            return reader;
        }
        
    }
}
