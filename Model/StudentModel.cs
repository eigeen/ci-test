﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfTest2_MVVM.Model
{
    class StudentModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public int Age { get; set; }
        public double Score { get; set; }
        public double GPA { get; set; }
    }
}
